//
//  ViewController.swift
//  MyApp
//
//  Created by Mavin on 9/7/20.
//  Copyright © 2020 hrd. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var operatorlbl: UILabel!
    @IBOutlet weak var resultLbl: UILabel!
    @IBOutlet weak var lblText: UILabel!
    
    var currentOpearator: String = ""
    var currentStr : String = ""
    var sign: String = ""
    var dotSign:Bool = false
    var firstValue: Double = 0.0
    var secondValue: Double = 0.0
    var result: Double = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func clearBtn(_ sender: Any) {
        resultLbl.text = "0"
        currentStr = ""
        lblText.text = ""
        operatorlbl.text = ""
        currentOpearator = ""
    }
    
    @IBAction func calculate(_ sender: UIButton) {
        
        sign = sender.currentTitle!
        firstValue = Double(resultLbl.text!)!
        lblText.text = resultLbl.text
        operatorlbl.text = resultLbl.text
        currentStr = ""
        operatorlbl.text = sender.currentTitle!
        resultLbl.text = "0"
                
    }
    
    @IBAction func number(_ sender: UIButton) {
        
//        if resultLbl.text! == "0" {
//            currentStr =  sender.currentTitle!
//        }else{
//            currentStr = resultLbl.text! + sender.currentTitle!
//        }
        
        if resultLbl.text! == "0"{
            currentStr = sender.currentTitle!
        }else{
            currentStr = resultLbl.text! + sender.currentTitle!
        }
        if currentStr.count < 10 {
            resultLbl.text = currentStr
        }else{
            resultLbl.text = resultLbl.text!
        }
      
        //tenary style
        
//        resultLbl.text = currentStr.count < 3 ? currentStr : resultLbl.text!
        
    
    }
    
    @IBAction func dotBtn(_ sender: UIButton) {
        print(dotSign)
//        resultLbl.text = resultLbl.text! + sender.currentTitle!
        let d = resultLbl.text!
        for hasdot in d  {
            if hasdot == "." {
                dotSign = true
            }
        }
        print(dotSign)
        if dotSign {
            resultLbl.text = resultLbl.text!
        }else{
            resultLbl.text = resultLbl.text! + sender.currentTitle!
        }

    }
    @IBAction func MinusPlus(_ sender: Any) {
//        var strResult: Double = 0.0
//        strResult = Double(resultLbl.text!)!
//        print(strResult)
        resultLbl.text = String(Double(resultLbl.text!)! * (-1))
    }
    @IBAction func equalBtn(_ sender: UIButton) {
        
        secondValue = Double(resultLbl.text!)!
        
        switch sign {
        case "+":
            result = firstValue + secondValue
            resultLbl.text = String(result)
        case "-":
            result = firstValue - secondValue
            resultLbl.text = String(result)
        case "x":
            result = firstValue * secondValue
            resultLbl.text = String(result)
        case "/":
            result = firstValue / secondValue
            resultLbl.text = String(result)
        case "%":
            result = Double(Int(firstValue) % Int(secondValue))
            resultLbl.text = String(result)
        default:
            print("default")
        }
        
    }
    
}

